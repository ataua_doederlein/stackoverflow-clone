# Clone do Stack Overflow em React + Docker
Como desafio pessoal, resolvi fazer um clone da página inicial do [Stack overflow](https://stackoverflow.com/) em React.
O projeto em si é mais uma desculpa para aprender a desenvolver um projeto React com Docker.

## Rodar projeto:  
1. Sem o Docker Compose:
```bash

# pra não precisar do *sudo* 
docker login

# build
docker build -t sample:dev .

# run
docker run -v ${PWD}:/app -it -v /app/node_modules -p 3001:3000 --rm sample:dev
```

2. Com Docker Compose:
```bash
# roda o container
docker-compose up -d --build

# para o container
docker-compose stop
```

## Referências:  
- [Dockerizando app React](https://blog.codeexpertslearning.com.br/dockerizando-uma-aplica%C3%A7%C3%A3o-react-js-f6a22e93bc5d)
- [React Bootstrap](https://react-bootstrap.github.io/)