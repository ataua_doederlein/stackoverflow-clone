import { Header } from "./components/Header";

function App() {
    return (
        <div className="App">

            <Header />

            <main>
              <h1>Main</h1>
            </main>

            <footer>
              <h3>Footer</h3>
            </footer>

        </div>
    );
}

export default App;
